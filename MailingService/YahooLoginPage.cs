﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using static System.Net.Mime.MediaTypeNames;

namespace MailingService
{
    public class YahooLoginPage
    {
        public IWebDriver driver;
        readonly By yahooUserName = By.XPath("//input[@id='login-username']");
        readonly By btnSignIn1 = By.XPath("//input[@id='login-signin']");
        readonly By yahooPassword = By.XPath("//input[@id='login-passwd']");
        readonly By btnSignIn2 = By.XPath("//button[@id='login-signin']");        

        public YahooLoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        //set userName in textbox
        public void SetUserName(string strUserName)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(yahooUserName)).SendKeys(strUserName);
        }

        //set password in textbox
        public void SetPassword(string strPassword)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(yahooPassword)).SendKeys(strPassword);
        }

        //click on Sing in button
        public void ClickSignIn(By signIn)
        {
            driver.FindElement(signIn).Click();
        }

        public void LoginToYahoo(string strUserName, string strPassword)
        {
            this.SetUserName(strUserName);
            this.ClickSignIn(btnSignIn1);
            this.SetPassword(strPassword);
            this.ClickSignIn(btnSignIn2);
        }

    }
}
