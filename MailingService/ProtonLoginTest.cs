global using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection.Metadata;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.DevTools;
using SeleniumExtras.WaitHelpers;

namespace MailingService
{
    public class ProtonLoginTest
    {
        public IWebDriver driver;
        public IWebDriver? driver2 = null;
        public ProtonLoginPage? objLoginProton = null;
        public ProtonManegersPage? objManagerPage = null;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://account.proton.me/mail");
            driver.Manage().Window.Maximize();
        }

        [Test]
        [TestCase(Constants.protonUser, Constants.protonPasswd, true)]
        [TestCase("", Constants.protonPasswd, false)]
        [TestCase(Constants.protonUser, "", false)]
        [TestCase("AbcDcb123@proton.me", Constants.protonPasswd, false)]
        [TestCase(Constants.protonUser, "12325f_dfdsk42", false)]
        public void ProtonLogin_ValidLoginOnlyWithCorrectCredentials(string username, string password, bool expectedResult)
        {
            //create object for the login page
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            objLoginProton = new ProtonLoginPage(driver);

            //verify login page title
            string loginPageTitle = objLoginProton.GetLoginTitle();
            Assert.That(loginPageTitle.ToLower(), Does.Contain("sign in to continue to proton mail"));

            //login to application
            objLoginProton.LoginToProton(username, password);

            //verify Managers page
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            objManagerPage = new ProtonManegersPage(driver);
            string? actualResult = objManagerPage.GetManagerPageMsg();

            if (actualResult is null)
            {
                Assert.That(expectedResult, Is.EqualTo(false));
            }
            else
            {
                Assert.That(actualResult.ToLower(), Does.Contain("inbox dr light-green"));
            }
        }

        [Test]
        public void ProtonLogin_SendMail()
        {
            string subject = "Test email " + GenerateNum();
            string body = "Test body " + GenerateNum();
            
            /*Send letter from email_1*/
            SendMail(driver, subject, body);

            driver2 = new ChromeDriver();
            driver2.Navigate().GoToUrl("https://login.yahoo.com/");
            driver2.Manage().Window.Maximize();

            string newNickname = "New nickname: Dr Light-Green " + GenerateNum();
            /*Check letter from email_1 at email_2*/
            ReceiveMail(driver2, subject, body, newNickname);

            ChangeNickname(driver, subject);
        }

        [TearDown]
        public void Quit()
        {
            driver.Quit();

            if (driver2 is not null)
                driver2.Quit();
        }

        private void SendMail(IWebDriver driver, string subject, string body)
        {
            //create object for the login page
            objLoginProton = new ProtonLoginPage(driver);

            //verify login page title
            string loginPageTitle = objLoginProton.GetLoginTitle();
            Assert.That(loginPageTitle.ToLower(), Does.Contain("sign in to continue to proton mail"));

            //login to application
            objLoginProton.LoginToProton(Constants.protonUser, Constants.protonPasswd);

            //Click New Message
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//button[text()='New message']"))).Click();

            //Email To
            driver.FindElement(By.XPath("//input[@data-testid='composer:to']")).SendKeys(Constants.yahooUser);

            //Subject
            IWebElement subj = driver.FindElement(By.XPath("//input[@data-testid='composer:subject']"));
            subj.SendKeys(subject);
            subj.SendKeys(Keys.Tab);

            //Body
            new Actions(driver).SendKeys(body).Perform();

            //Send
            driver.FindElement(By.XPath("//button[@data-testid='composer:send-button']")).Click();
            Thread.Sleep(60000);
        }

        private static void ReceiveMail(IWebDriver driver, string subject, string body, string nickname)
        {
            YahooLoginPage objLoginYahoo = new(driver);

            //login to application
            objLoginYahoo.LoginToYahoo(Constants.yahooUser, Constants.yahooPasswd);
            
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

            //Open mail
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//a[@id='ybarMailLink']"))).Click();

            //Waiting for the new letter
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(500));
            IWebElement letter = wait.Until(ExpectedConditions.ElementExists(By.XPath("//button[@data-test-id='icon-btn-read']")));

            //Letter is unread 
            Assert.That(letter.GetAttribute("title").ToString(), Is.EqualTo("Mark as read"));
            //Letter has the correct sender
            Assert.That(driver.FindElement(By.XPath("//span[@data-test-id='senders_list']")).GetAttribute("title").ToString(), Is.EqualTo(Constants.protonUser.ToLower()));
            //Letter has the correct subject
            Assert.That(driver.FindElement(By.XPath("//span[@data-test-id='message-subject']")).Text, Is.EqualTo(subject));
            
            //Open letter
            driver.FindElement(By.XPath("//span[@data-test-id='message-subject']")).Click();
            //Letter has the correct body
            Assert.That(driver.FindElement(By.XPath("//div[@data-test-id='message-view-body-content']/div/div/div/div[1]")).Text, Is.EqualTo(body));
            //Reply
            driver.FindElement(By.XPath("//button[@data-kind='reply']")).Click();

            //Input and send reply
            new Actions(driver).SendKeys(nickname + ".").Perform();
            new Actions(driver).SendKeys(Keys.Enter).Perform();
            new Actions(driver).SendKeys("Thank you for your email!").Perform();
            driver.FindElement(By.XPath("//button[@data-test-id='compose-send-button']")).Click();
            Thread.Sleep(5000);
        }

        private static void ChangeNickname(IWebDriver driver, string subject)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(500));
            string xpath = "//span[contains(@title,'" + subject + "')]";
            IWebElement letter = wait.Until(ExpectedConditions.ElementExists(By.XPath(xpath)));
            //Open letter;
            letter.Click();
            driver.FindElement(By.XPath("//button[@data-testid='message-header-expanded:more-dropdown']")).Click();
            driver.FindElement(By.XPath("//button[@data-testid='message-view-more-dropdown:view-html']")).Click();
            //Finding new nickname from html (cause of sendbox in iframe)
            string bodyText = driver.FindElement(By.XPath("//div[@data-testid='message-content:body']/pre")).Text;
            string nickname = bodyText.Substring(bodyText.IndexOf("New nickname"), 100);
            nickname = nickname.Substring(nickname.IndexOf(":"));
            nickname = nickname.Substring(2, nickname.IndexOf("<") - 2);

            //Open settings
            driver.FindElement(By.XPath("//button[@data-testid='heading:userdropdown']/span[2]")).Click();
            driver.FindElement(By.XPath("//button[@data-testid='userdropdown:button:settings']")).Click();
            //Account and password
            wait.Until(ExpectedConditions.ElementExists(By.XPath("//span[@title='Account and password']/../../.."))).Click();
            //Edit display name
            driver.FindElement(By.XPath("//button[@aria-label='Edit display name']")).Click();
            //Enter new nickname
            driver.FindElement(By.Id("displayName")).SendKeys(nickname);
            //Save
            driver.FindElement(By.XPath("//button[@type='submit']")).SendKeys(nickname);
            Thread.Sleep(2000);
            //Check changes
            Assert.That(driver.FindElement(By.XPath("//button[@data-testid='heading:userdropdown']/span[1]/span[1]")).Text, 
                Is.EqualTo(nickname));
        }        

        private static string GenerateNum()
        {
            Random rnd = new();
            return rnd.Next(10, 100).ToString();
        }
    }
}