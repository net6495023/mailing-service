﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailingService
{
    public class ProtonManegersPage
    {
        public IWebDriver driver;
        readonly By managerPageMsg1 = By.XPath("//h2[@class='text-lg text-bold text-ellipsis']");
        readonly By managerPageMsg2 = By.XPath("//span[@class='block text-ellipsis text-sm user-dropdown-displayName']");

        public ProtonManegersPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public string? GetManagerPageMsg()
        {
            try 
            {
                return driver.FindElement(managerPageMsg1).Text + " " + driver.FindElement(managerPageMsg2).Text;
            }
            catch(NoSuchElementException) 
            {
                return null;
            }
        }
    }
}
