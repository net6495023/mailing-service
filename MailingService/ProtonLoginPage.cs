﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using static System.Net.Mime.MediaTypeNames;

namespace MailingService
{
    public class ProtonLoginPage
    {
        public IWebDriver driver;
        readonly By protonUserName = By.Id("username");
        readonly By protonPassword = By.Id("password");
        readonly By signInText = By.XPath("//h1[@class='sign-layout-title']");
        readonly By signInText2 = By.XPath("//div[@class='mt-2 color-weak']");
        readonly By btnSignIn = By.XPath("//button[text()='Sign in']");

        public ProtonLoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }

        //set userName in textbox
        public void SetUserName(string strUserName)
        {
            driver.FindElement(protonUserName).SendKeys(strUserName);
        }

        //set password in textbox
        public void SetPassword(string strPassword)
        {
            driver.FindElement(protonPassword).SendKeys(strPassword);
        }

        //click on Sing in button
        public void ClickSignIn()
        {
            driver.FindElement(btnSignIn).Click();
        }

        //get the title of the login page
        public string GetLoginTitle()
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            return wait.Until(ExpectedConditions.ElementExists(signInText)).Text
                + " " + wait.Until(ExpectedConditions.ElementExists(signInText2)).Text;
        }

        public void LoginToProton(string strUserName, string strPassword)
        {
            this.SetUserName(strUserName);
            this.SetPassword(strPassword);
            this.ClickSignIn();
        }

    }
}
